% To create the Dataset

% Author: Ma, yifan (yifan.ma@mailbox.tu-dresden.de)
% Date: 17.06.2021
% The MIT License
% Copyright (c) [2021] Ma yifan
clc
clear
close all

%% Variables Setting
p_Trn =0.8; % proportion of traning in a_Trn
img_resize_scale = 1;
lbl_resize_scale = 1;
Loadfile1='Dataset/mnistL32_trn.mat';
Loadfile2='Dataset/mnistL32_test.mat';
Savefile='Dataset/fashion_L_5000';
%% Load raw Training Dataset
load(Loadfile1);

a_Trn=size(Images,4); % amount for training and validation

Images = imresize(Images,img_resize_scale);
Labels = imresize(Labels,lbl_resize_scale);

img_Trn   = Images(:,:,:,1:p_Trn*a_Trn);
img_Vld  = Images(:,:,:,p_Trn*a_Trn+1:a_Trn);
lbl_Trn  = Labels(:,:,:,1:p_Trn*a_Trn);
lbl_Vld  = Labels(:,:,:,p_Trn*a_Trn+1:a_Trn);


%% Load raw Test Dataset
load(Loadfile2);

a_Test=size(Images,4); % amount for test
Images = imresize(Images,img_resize_scale);
Labels = imresize(Labels,lbl_resize_scale);
img_Test  = Images;
lbl_Test  = Labels;

%% Create mixed Dataset
% load Dataset/mnist_L_trn.mat
% img_temp = Images;
% lbl_temp = Labels;
% load Dataset/fashion_L_trn.mat
% img = cat(4,Images,img_temp);
% lbl = cat(4,Labels,lbl_temp);
% 
% a_Trn=size(Images,4)*2; % amount for training and validation
% idx=randperm(a_Trn);
% 
% img = img(:,:,layers,idx);
% lbl = lbl(:,:,layers,idx);
% 
% img_Trn   = img(:,:,layers,1:p_Trn*a_Trn);
% img_Vld  = img(:,:,layers,p_Trn*a_Trn+1:a_Trn);
% lbl_Trn  = lbl(:,:,layers,1:p_Trn*a_Trn);
% lbl_Vld  = lbl(:,:,layers,p_Trn*a_Trn+1:a_Trn);
% 
% load Dataset/mnist_L_test.mat
% img_temp = Images;
% lbl_temp = Labels;
% load Dataset/fashion_L_test.mat
% img = cat(4,Images,img_temp);
% lbl = cat(4,Labels,lbl_temp);
% 
% a_Test=size(Images,4)*2; % amount for test
% Tidx=randperm(a_Test);
% 
% img = img(:,:,layers,Tidx);
% lbl = lbl(:,:,layers,Tidx);
% 
% img_Test  = img;
% lbl_Test  = lbl;

%% Figure Example
figure
tiledlayout(2,5,'TileSpacing','none')
for i=1:5
    nexttile
    imshow(img_Test(:,:,1,i),[])
end
title('Input')
for j=1:5
    nexttile
    imshow(lbl_Test(:,:,1,j),[])
end
title('Label')

%% Save Dataset
[~,~]=mkdir('Dataset');
save(Savefile,'img_Trn','img_Vld','img_Test','lbl_Trn','lbl_Vld','lbl_Test','-v7.3');