% Script to train the defined neural networks(U-net)

% Author: Ma, yifan (yifan.ma@mailbox.tu-dresden.de) 
% Date: 23.06.2021
% The MIT License
% Copyright (c) [2021] Ma yifan

clc
for n=1:1 % Loop for retrain
% clc
clear
close all

%% Variables
LoadNet=1; % if you want to load the networks form document, set 1, otherwise 0
NetFile='Network/DenseNet.mat'; % The file path of the target network
numEpochs = 50; % Epochs in training
miniBatchSize = 80; % set Batchsize
initialLearnRate = 1e-4; % 1e-4 for mse 
validationFrequency = 50; % The frequency of validation results on the plot
%% Load Dataset
load Dataset/mnist_L.mat
% img_Trn : Training images
% img_Vld : Validation images
% img_Test: Test images
% lbl_Trn : Training labels
% lbl_Vld : Validation labels
% lbl_Test: Test labels
inputsize=size(img_Trn,1:3);% input feature map
filter_num=64; %filter number
filter_size=[3 3];%filter size
layers = 1;
dsXTrain = arrayDatastore(img_Trn,'IterationDimension',4);
dsYTrain = arrayDatastore(lbl_Trn,'IterationDimension',4);
dsTrain = combine(dsXTrain,dsYTrain);

dsXVld = arrayDatastore(img_Vld,'IterationDimension',4);
dsYVld = arrayDatastore(lbl_Vld,'IterationDimension',4);
dsVld = combine(dsXVld,dsYVld);
clear dsXTrain dsYTrain dsXVld dsYVld
%% U-Net Layer
lgraph = layerGraph();

tempLayers = [
    imageInputLayer(inputsize,'Normalization','none',"Name","ImageInputLayer")
    convolution2dLayer(filter_size,filter_num,"Name","Encoder-Stage-1-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-1-ReLU-1")
    convolution2dLayer(filter_size,filter_num,"Name","Encoder-Stage-1-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-1-ReLU-2")
    maxPooling2dLayer([2 2],"Name","Encoder-Stage-1-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*2,"Name","Encoder-Stage-2-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-2-ReLU-1")
    convolution2dLayer(filter_size,filter_num*2,"Name","Encoder-Stage-2-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-2-ReLU-2")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    maxPooling2dLayer([2 2],"Name","Encoder-Stage-2-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*4,"Name","Encoder-Stage-3-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-3-ReLU-1")
    convolution2dLayer(filter_size,filter_num*4,"Name","Encoder-Stage-3-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-3-ReLU-2")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    maxPooling2dLayer([2 2],"Name","Encoder-Stage-3-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*8,"Name","Encoder-Stage-4-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-4-ReLU-1")
    convolution2dLayer(filter_size,filter_num*8,"Name","Encoder-Stage-4-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-4-ReLU-2")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    maxPooling2dLayer([2 2],"Name","Encoder-Stage-4-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*16,"Name","Bridge-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Bridge-ReLU-1")
    convolution2dLayer(filter_size,filter_num*16,"Name","Bridge-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Bridge-ReLU-2")
    transposedConv2dLayer([2 2],filter_num*8,"Name","Decoder-Stage-1-UpConv","BiasLearnRateFactor",2,"Stride",[2 2],"WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-1-UpReLU")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    depthConcatenationLayer(2,"Name","Decoder-Stage-1-DepthConcatenation")
    convolution2dLayer(filter_size,filter_num*8,"Name","Decoder-Stage-1-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-1-ReLU-1")
    convolution2dLayer(filter_size,filter_num*8,"Name","Decoder-Stage-1-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-1-ReLU-2")
    transposedConv2dLayer([2 2],filter_num*4,"Name","Decoder-Stage-2-UpConv","BiasLearnRateFactor",2,"Stride",[2 2],"WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-2-UpReLU")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    depthConcatenationLayer(2,"Name","Decoder-Stage-2-DepthConcatenation")
    convolution2dLayer(filter_size,filter_num*4,"Name","Decoder-Stage-2-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-2-ReLU-1")
    convolution2dLayer(filter_size,filter_num*4,"Name","Decoder-Stage-2-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-2-ReLU-2")
    transposedConv2dLayer([2 2],filter_num*2,"Name","Decoder-Stage-3-UpConv","BiasLearnRateFactor",2,"Stride",[2 2],"WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-3-UpReLU")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    depthConcatenationLayer(2,"Name","Decoder-Stage-3-DepthConcatenation")
    convolution2dLayer(filter_size,filter_num*2,"Name","Decoder-Stage-3-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-3-ReLU-1")
    convolution2dLayer(filter_size,filter_num*2,"Name","Decoder-Stage-3-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-3-ReLU-2")
    dropoutLayer("Name","DropOut")% Dropout Layer
    convolution2dLayer(filter_size,filter_num,"Name","Decoder-Stage-4-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-4-ReLU-1")
    convolution2dLayer(filter_size,filter_num,"Name","Decoder-Stage-4-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-4-ReLU-2")
    convolution2dLayer([1 1],layers,"Name","Final-ConvolutionLayer")];
lgraph = addLayers(lgraph,tempLayers);

% clear up helper variable
clear tempLayers;

lgraph = connectLayers(lgraph,"Encoder-Stage-2-ReLU-2","Encoder-Stage-2-MaxPool");
lgraph = connectLayers(lgraph,"Encoder-Stage-2-ReLU-2","Decoder-Stage-3-DepthConcatenation/in2");
lgraph = connectLayers(lgraph,"Encoder-Stage-3-ReLU-2","Encoder-Stage-3-MaxPool");
lgraph = connectLayers(lgraph,"Encoder-Stage-3-ReLU-2","Decoder-Stage-2-DepthConcatenation/in2");
lgraph = connectLayers(lgraph,"Encoder-Stage-4-ReLU-2","Encoder-Stage-4-MaxPool");
lgraph = connectLayers(lgraph,"Encoder-Stage-4-ReLU-2","Decoder-Stage-1-DepthConcatenation/in2");
lgraph = connectLayers(lgraph,"Decoder-Stage-1-UpReLU","Decoder-Stage-1-DepthConcatenation/in1");
lgraph = connectLayers(lgraph,"Decoder-Stage-2-UpReLU","Decoder-Stage-2-DepthConcatenation/in1");
lgraph = connectLayers(lgraph,"Decoder-Stage-3-UpReLU","Decoder-Stage-3-DepthConcatenation/in1");

if LoadNet==1
    load(NetFile);
end

dlnet=dlnetwork(lgraph);

%% Set Batchqueue for training set and validation set
mbq = minibatchqueue(dsTrain,...
    'MiniBatchSize',miniBatchSize,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});

mbqV = minibatchqueue(dsVld,...
    'MiniBatchSize',miniBatchSize,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});

clear dsTrain dsVld
%% Plot setting
f = figure;
f.Position(3) = 2*f.Position(3);
imageAxes = subplot(1,2,1);
scoreAxes = subplot(1,2,2);
lineLossTrain = animatedline('Color','#D95319','LineWidth',1);
lineLossValid = animatedline('Color','#0072BD','LineWidth',1);
ylim([0 inf])
xlabel("Iteration")
ylabel("Loss")
grid on
%% Training Loop
iteration = 0;
start = tic;
averageGrad = [];
averageSqGrad = [];

% Loop over epochs.
for epoch = 1:numEpochs
    % Shuffle data.
    shuffle(mbq);
    
    % Loop over mini-batches.
    while hasdata(mbq)
        iteration = iteration + 1;
        
        % Read mini-batch of data.
        [dlX, dlY] = next(mbq);
        % Evaluate the model gradients, state, and loss using dlfeval and the
        % modelGradients function and update the network state.
        [gradients,state,loss] = dlfeval(@modelGradients,dlnet,dlX,dlY);
        dlnet.State = state; 
        
        % Determine learning rate for time-based decay learning rate schedule.
%       learnRate = initialLearnRate/(1 + decay*iteration);
        
        % Update the network parameters using the ADAM optimizer.
        [dlnet,averageGrad,averageSqGrad] = adamupdate(dlnet,gradients,averageGrad,averageSqGrad,iteration,...
                                                       initialLearnRate);
        
        
        
        
        % Display the training progress.
        subplot(1,2,2)
        addpoints(lineLossTrain,iteration,loss)
        if mod(iteration,validationFrequency) == 0 || iteration == 1
            shuffle(mbqV)
            [dlXValid,dlYValid] = next(mbqV);
            Validloss = modelPredictions(dlnet,dlXValid,dlYValid);
            addpoints(lineLossValid,iteration,Validloss)
            % Tile and rescale the images in the range [0 1].
            dlYpred=predict(dlnet,dlXValid);
            I = imtile(extractdata(dlYpred(:,:,1,:)));
            I = rescale(I);
            subplot(1,2,1);
            imshow(I,[]);
            xticklabels([]);
            yticklabels([]);
            title("Generated Images");
        end
        D = duration(0,0,toc(start),'Format','hh:mm:ss');
        title("Epoch: " + epoch + ", Iteration: " + iteration + ", Elapsed: " + string(D))
        drawnow
        
    end
end
fprintf("Time:%s\n",D);
%% Evaluate
Labels=lbl_Test; 
Pre=zeros(size(lbl_Test),'single');
ccTest = [];
ssimval = [];
peaksnr = [];
nbytes=0;
fprintf("Evaluation the results...\n");
for i =1:size(img_Test,4)
    dlYPred= predict(dlnet,dlarray(img_Test(:,:,1,i),'SSCB'));
    dlYPred=reshape(dlYPred,64,64);
    dlYPred(dlYPred<0)=0;
    pre=(gather(extractdata(dlYPred./max(dlYPred,[],'all')*255)));
    Pre(:,:,:,i)=pre;
    %NPCC
    rou=corrcoef(Labels(:,:,1,i),Pre(:,:,1,i));
    ccTest=[ccTest rou(1,2)];
    %SSIM
    ssimvaltemp=ssim(uint8(Labels(:,:,1,i)),uint8(Pre(:,:,1,i)));
    ssimval = [ssimval ssimvaltemp];
    %PSNR
    psnrtemp= psnr(uint8(Pre(:,:,1,i)),uint8(Labels(:,:,1,i)));
    peaksnr =[peaksnr psnrtemp];
    
    fprintf(repmat('\b',1,nbytes)); 
    nbytes = fprintf([num2str(i) '/' num2str(size(img_Test,4))]);
end
fprintf('\n');
fprintf('NPCC of Test dataset is:%.3f', sum(ccTest)./size(img_Test,4));
fprintf('\n');
fprintf('SSIM of Test dataset is:%.3f', sum(ssimval)./size(img_Test,4));
fprintf('\n');
fprintf('PSNR of Test dataset is:%.3f', sum(peaksnr)./size(img_Test,4));
fprintf('\n');
%% Save Trained Net
[~,~]=mkdir('TrainedNet');
% save('TrainedNet/densenet-10k','dlnet');

%% Test Example
k=randperm(size(img_Test,4),5);
figure
tiledlayout(2,5,'TileSpacing','none')
for i=k
    nexttile
    imshow(Labels(:,:,1,i),[])
end
title('Label')
for j=k
    nexttile
    imshow(Pre(:,:,1,j),[])
end
title('Prediction')

end
%% Loss Function

function loss = LossFcn(Yp,Y)
%     loss = SSIMLoss(Yp,Y)+mseLoss(Yp,Y);
    loss = mseLoss(Yp,Y);
%     loss=SSIMLoss(Yp,Y);
end

% normolized mse
function loss = mseLoss(Yp,Y)

Yp=dlarray(Yp,'SSCB');
for i = 1:size(Y,4)
    Y(:,:,:,i)=Y(:,:,:,i)./sqrt(sum(Y(:,:,:,i).^2,'all'));
    Yp(:,:,:,i)=Yp(:,:,:,i)./sqrt(sum(Yp(:,:,:,i).^2,'all')); 
end

loss = mse(Yp,Y);
end

% ssim

function ssim_loss=SSIMLoss(predict,truth)
dynmRange = 255;
C = [(0.01*dynmRange).^2 (0.03*dynmRange).^2 ((0.03*dynmRange).^2)/2];

radius=1.5;
filtRadius = ceil(radius*3); % 3 Standard deviations include >99% of the area.
filtSize = 2*filtRadius + 1;

h = fspecial('gaussian',filtSize,radius);
gaussFilterFcn = @(X)dlconv(X,h,0,'Padding','same');

mux = gaussFilterFcn(truth);
muy2 = gaussFilterFcn(predict);
muxy = mux.*muy2;
mux2 = mux.^2;
muy2 = muy2.^2;

sigmax2 = max(gaussFilterFcn(truth.^2) - mux2,0);
sigmay2 = max(gaussFilterFcn(predict.^2) - muy2,0);

sigmaxy = gaussFilterFcn(truth.*predict) - muxy;

num = (2*muxy + C(1)).*(2*sigmaxy + C(2));
den = (mux2 + muy2 + C(1)).*(sigmax2 + sigmay2 + C(2));
ssimmap = num./den.*truth/255;
ssim_loss =mean(ssimmap(ssimmap~=0));
ssim_loss = 1-ssim_loss;
end
%% Calculate Gradient 
function [gradients,state,loss] = modelGradients(dlnet,dlX,Y)

[dlYPred,state] = forward(dlnet,dlX);

loss = LossFcn(dlYPred,Y);
gradients = dlgradient(loss,dlnet.Learnables);

loss = double(gather(extractdata(loss)));

end
%% Validation Loss
function Validloss = modelPredictions(dlnet,dlXValid,dlYValid)
    
    Validloss=0;
    i=0;
%     while hasdata(mbq)
        dlYPred = predict(dlnet,dlXValid);
        loss = LossFcn(dlYPred,dlYValid);
        i=i+1;
        Validloss = Validloss+double(gather(extractdata(loss)));
%     end
    
    Validloss = Validloss./i;
end
%% minibatch process
function [X,Y] = preprocessMiniBatch(XCell,YCell)
    
    % Extract image data from cell and concatenate
    X = cat(4,XCell{:});
    % Extract label data from cell and concatenate
    Y = cat(4,YCell{:});

end