% This skript works with experimental data from Marco

% Author: Ma, yifan (yifan.ma@mailbox.tu-dresden.de)
% Date: 14.09.2021
% The MIT License
% Copyright (c) [2021] Ma yifan

clc
clear
close all

%% Variables
I_pix=128; % pixel Images
L_pix=64; % pixel Labels
color=3;

p_Trn =0.8; % proportion of traning in a_Trn
p_Vld=0.1;
%% File path setting
path=fullfile('ExpData','600','ground truth','blau'); % path to clothing images
path2=fullfile('ExpData','600','speckle','blau');
imgList=dir(fullfile(path,'**/*.JPEG'));
imgList_img=dir(fullfile(path2,'**/*.JPEG'));
img_max=size(imgList,1);
%% Create Training Objects in multiple layers
n=randperm(img_max);
for i=1:img_max
    img1=imread(fullfile(imgList_img(n(i)).folder,imgList_img(n(i)).name)); % load speckle patterns
    img1=imresize(img1,[I_pix,I_pix]);
    Images(:,:,:,i)=img1;
    img2=imread(fullfile(imgList(n(i)).folder,imgList(n(i)).name)); % load ground truth images
    img2=imresize(img2(:,:,color),[L_pix,L_pix]);
    Labels(:,:,:,i)=img2;
    disp(i)
end

img_Trn   = Images(:,:,:,1:fix(p_Trn*img_max));
img_Vld  = Images(:,:,:,fix(p_Trn*img_max)+1:fix((p_Trn+p_Vld)*img_max));
img_Test  = Images(:,:,:,fix((p_Trn+p_Vld)*img_max)+1:end);
lbl_Trn  = Labels(:,:,:,1:fix(p_Trn*img_max));
lbl_Vld  = Labels(:,:,:,fix(p_Trn*img_max)+1:fix((p_Trn+p_Vld)*img_max));
lbl_Test  = Labels(:,:,:,fix((p_Trn+p_Vld)*img_max)+1:end);

%% Save File
save('Dataset/ExpData_600','img_Trn','img_Vld','img_Test','lbl_Trn','lbl_Vld','lbl_Test','-v7.3');