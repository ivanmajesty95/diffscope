% The Script is for fcn related training tasks

% Author: Ma, yifan (yifan.ma@mailbox.tu-dresden.de) 
% Date: 06.09.2021
% The MIT License
% Copyright (c) [2021] Ma yifan
clc
for n=1:1 % Loop for retrain
% clc
clear
close all

%% Training setting
unet_on=0; % 0 for skip unet training, only FCN works

numEpochs_fcn = 50;
miniBatchSize_fcn = 100;
initialLearnRate_fcn = 1e-4;

numEpochs_unet = 30;
miniBatchSize_unet = 100;
initialLearnRate_unet = 1e-4;

validationFrequency = 100;
%% Load Dataset
load Dataset/mnist_L.mat
% img_Trn : Training images
% img_Vld : Validation images
% img_Test: Test images
% lbl_Trn : Training labels
% lbl_Vld : Validation labels
% lbl_Test: Test labels
inputsize=[128 128 1];
outputsize=[64 64 1];
filter_num=64;
filter_size=[3 3];
% layers = 1;
dsXTrain = arrayDatastore(img_Trn,'IterationDimension',4);
dsYTrain = arrayDatastore(lbl_Trn,'IterationDimension',4);
dsTrain = combine(dsXTrain,dsYTrain);

dsXVld = arrayDatastore(img_Vld,'IterationDimension',4);
dsYVld = arrayDatastore(lbl_Vld,'IterationDimension',4);
dsVld = combine(dsXVld,dsYVld);
clear dsXTrain dsYTrain dsXVld dsYVld
%% FCN Layer
lgraph1 = layerGraph();

tempLayers = [
    imageInputLayer(inputsize,'Normalization','none',"Name","ImageInputLayer")
    fullyConnectedLayer(4096,"Name","fc1")
    fullyConnectedLayer(4096,"Name","fc2")
    ReshapeLayer([64 64 1],'Reshape')
%     convolution2dLayer([3 3],32,"Name","Filter1","Padding","same","WeightsInitializer","he")
%     convolution2dLayer([3 3],1,"Name","Filter2","Padding","same")
                ];
lgraph1 = addLayers(lgraph1,tempLayers);

dlnet1=dlnetwork(lgraph1);

%% Set Batchqueue for training set and validation set
mbq = minibatchqueue(dsTrain,...
    'MiniBatchSize',miniBatchSize_fcn,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});

mbqV = minibatchqueue(dsVld,...
    'MiniBatchSize',miniBatchSize_fcn,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});

clear dsTrain dsVld
%% Plot setting
f = figure;
f.Position(3) = 2*f.Position(3);
imageAxes = subplot(1,2,1);
scoreAxes = subplot(1,2,2);
lineLossTrain1 = animatedline('Color','#D95319','LineWidth',1);
lineLossValid1 = animatedline('Color','#0072BD','LineWidth',1);
ylim([0 inf])
xlabel("Iteration")
ylabel("Loss")
grid on
%% FCN Training Loop
iteration = 0;
start = tic;
averageGrad = [];
averageSqGrad = [];
% Loop over epochs.
for epoch = 1:numEpochs_fcn
    % Shuffle data.
    shuffle(mbq);
    
    % Loop over mini-batches.
    while hasdata(mbq)
        iteration = iteration + 1;
        
        % Read mini-batch of data.
        [dlX, dlY] = next(mbq);
        % Evaluate the model gradients, state, and loss using dlfeval and the
        % modelGradients function and update the network state.
        [gradients1,state,loss1] = dlfeval(@modelGradients,dlnet1,dlX,dlY);
        dlnet1.State = state;
        
        % Determine learning rate for time-based decay learning rate schedule.
%       learnRate = initialLearnRate/(1 + decay*iteration);
        
        % Update the network parameters using the ADAM optimizer.
        [dlnet1,averageGrad,averageSqGrad] = adamupdate(dlnet1,gradients1,averageGrad,averageSqGrad,iteration,...
                                                       initialLearnRate_fcn);
        
        % Display the training progress.
        subplot(1,2,2)
        addpoints(lineLossTrain1,iteration,loss1)
        if mod(iteration,validationFrequency) == 0 || iteration == 1
            shuffle(mbqV)
            [dlXValid,dlYValid] = next(mbqV);
            dlYpred=predict(dlnet1,dlXValid);
            Validloss1 = modelPredictions(dlnet1,dlXValid,dlYValid);         
            addpoints(lineLossValid1,iteration,Validloss1)
            % Tile and rescale the images in the range [0 1].
            I = imtile(extractdata(dlYpred));
            I = rescale(I);
            subplot(1,2,1);
            imshow(I,[]);
            xticklabels([]);
            yticklabels([]);
            title("Generated Images");
        end
        D = duration(0,0,toc(start),'Format','hh:mm:ss');
        title("FCN Epoch: " + epoch + ", Iteration: " + iteration + ", Elapsed: " + string(D))
        drawnow
        
    end
end

%% U-Net Layer
if unet_skip==1
    
lgraph = layerGraph();

tempLayers = [
    imageInputLayer(outputsize,'Normalization','none',"Name","ImageInputLayer")
%     convolution2dLayer(filter_size,filter_num,"Name","Encoder-Stage-1-Conv-1","Padding","same","WeightsInitializer","he")
%     reluLayer("Name","Encoder-Stage-1-ReLU-1")
%     convolution2dLayer(filter_size,filter_num,"Name","Encoder-Stage-1-Conv-2","Padding","same","WeightsInitializer","he")
%     reluLayer("Name","Encoder-Stage-1-ReLU-2")
%     maxPooling2dLayer([2 2],"Name","Encoder-Stage-1-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*2,"Name","Encoder-Stage-2-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-2-ReLU-1")
    convolution2dLayer(filter_size,filter_num*2,"Name","Encoder-Stage-2-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-2-ReLU-2")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    maxPooling2dLayer([2 2],"Name","Encoder-Stage-2-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*4,"Name","Encoder-Stage-3-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-3-ReLU-1")
    convolution2dLayer(filter_size,filter_num*4,"Name","Encoder-Stage-3-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-3-ReLU-2")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    maxPooling2dLayer([2 2],"Name","Encoder-Stage-3-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*8,"Name","Encoder-Stage-4-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-4-ReLU-1")
    convolution2dLayer(filter_size,filter_num*8,"Name","Encoder-Stage-4-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Encoder-Stage-4-ReLU-2")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    maxPooling2dLayer([2 2],"Name","Encoder-Stage-4-MaxPool","Stride",[2 2])
    convolution2dLayer(filter_size,filter_num*16,"Name","Bridge-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Bridge-ReLU-1")
    convolution2dLayer(filter_size,filter_num*16,"Name","Bridge-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Bridge-ReLU-2")
    transposedConv2dLayer([2 2],filter_num*8,"Name","Decoder-Stage-1-UpConv","BiasLearnRateFactor",2,"Stride",[2 2],"WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-1-UpReLU")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    depthConcatenationLayer(2,"Name","Decoder-Stage-1-DepthConcatenation")
    convolution2dLayer(filter_size,filter_num*8,"Name","Decoder-Stage-1-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-1-ReLU-1")
    convolution2dLayer(filter_size,filter_num*8,"Name","Decoder-Stage-1-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-1-ReLU-2")
    transposedConv2dLayer([2 2],filter_num*4,"Name","Decoder-Stage-2-UpConv","BiasLearnRateFactor",2,"Stride",[2 2],"WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-2-UpReLU")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    depthConcatenationLayer(2,"Name","Decoder-Stage-2-DepthConcatenation")
    convolution2dLayer(filter_size,filter_num*4,"Name","Decoder-Stage-2-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-2-ReLU-1")
    convolution2dLayer(filter_size,filter_num*4,"Name","Decoder-Stage-2-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-2-ReLU-2")
    transposedConv2dLayer([2 2],filter_num*2,"Name","Decoder-Stage-3-UpConv","BiasLearnRateFactor",2,"Stride",[2 2],"WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-3-UpReLU")];
lgraph = addLayers(lgraph,tempLayers);

tempLayers = [
    depthConcatenationLayer(2,"Name","Decoder-Stage-3-DepthConcatenation")
    convolution2dLayer(filter_size,filter_num*2,"Name","Decoder-Stage-3-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-3-ReLU-1")
    convolution2dLayer(filter_size,filter_num*2,"Name","Decoder-Stage-3-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-3-ReLU-2")
    dropoutLayer("Name","DropOut")
    convolution2dLayer(filter_size,filter_num,"Name","Decoder-Stage-4-Conv-1","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-4-ReLU-1")
    convolution2dLayer(filter_size,filter_num,"Name","Decoder-Stage-4-Conv-2","Padding","same","WeightsInitializer","he")
    reluLayer("Name","Decoder-Stage-4-ReLU-2")
    convolution2dLayer([1 1],1,"Name","Final-ConvolutionLayer")];
lgraph = addLayers(lgraph,tempLayers);

% clear up helper variable
clear tempLayers;

lgraph = connectLayers(lgraph,"Encoder-Stage-2-ReLU-2","Encoder-Stage-2-MaxPool");
lgraph = connectLayers(lgraph,"Encoder-Stage-2-ReLU-2","Decoder-Stage-3-DepthConcatenation/in2");
lgraph = connectLayers(lgraph,"Encoder-Stage-3-ReLU-2","Encoder-Stage-3-MaxPool");
lgraph = connectLayers(lgraph,"Encoder-Stage-3-ReLU-2","Decoder-Stage-2-DepthConcatenation/in2");
lgraph = connectLayers(lgraph,"Encoder-Stage-4-ReLU-2","Encoder-Stage-4-MaxPool");
lgraph = connectLayers(lgraph,"Encoder-Stage-4-ReLU-2","Decoder-Stage-1-DepthConcatenation/in2");
lgraph = connectLayers(lgraph,"Decoder-Stage-1-UpReLU","Decoder-Stage-1-DepthConcatenation/in1");
lgraph = connectLayers(lgraph,"Decoder-Stage-2-UpReLU","Decoder-Stage-2-DepthConcatenation/in1");
lgraph = connectLayers(lgraph,"Decoder-Stage-3-UpReLU","Decoder-Stage-3-DepthConcatenation/in1");

dlnet2=dlnetwork(lgraph);
%% U-net Training Loop
img_Trnm=predict(dlnet1,dlarray(single(img_Trn),'SSCB'));
img_Vldm=predict(dlnet1,dlarray(single(img_Vld),'SSCB'));

dsXTrain = arrayDatastore(img_Trnm,'IterationDimension',4);
dsYTrain = arrayDatastore(lbl_Trn,'IterationDimension',4);
dsTrainm = combine(dsXTrain,dsYTrain);

dsXVld = arrayDatastore(img_Vldm,'IterationDimension',4);
dsYVld = arrayDatastore(lbl_Vld,'IterationDimension',4);
dsVldm = combine(dsXVld,dsYVld);
clear dsXTrain dsYTrain dsXVld dsYVld
% Set Batchqueue for training set and validation set
mbqm = minibatchqueue(dsTrainm,...
    'MiniBatchSize',miniBatchSize_unet,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});

mbqVm = minibatchqueue(dsVldm,...
    'MiniBatchSize',miniBatchSize_unet,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});
% Plot setting
f = figure;
f.Position(3) = 2*f.Position(3);
imageAxes1 = subplot(1,2,1);
scoreAxes1 = subplot(1,2,2);
lineLossTrain2 = animatedline('Color','r','LineWidth',1);
lineLossValid2 = animatedline('Color','green','LineWidth',1);
ylim([0 inf])
xlabel("Iteration")
ylabel("Loss")
grid on

iteration = 0;
start = tic;
averageGrad = [];
averageSqGrad = [];
% Loop over epochs.
for epoch = 1:numEpochs_unet
    % Shuffle data.
    shuffle(mbqm);
    
    % Loop over mini-batches.
    while hasdata(mbqm)
        iteration = iteration + 1;
        
        % Read mini-batch of data.
        [dlXm, dlY] = next(mbqm);
        % Evaluate the model gradients, state, and loss using dlfeval and the
        % modelGradients function and update the network state.
        [gradients2,state2,loss2] = dlfeval(@modelGradients,dlnet2,dlXm,dlY);
        dlnet2.State = state2;
        
        % Determine learning rate for time-based decay learning rate schedule.
%       learnRate = initialLearnRate/(1 + decay*iteration);
        
        % Update the network parameters using the ADAM optimizer.
        [dlnet2,averageGrad,averageSqGrad] = adamupdate(dlnet2,gradients2,averageGrad,averageSqGrad,iteration,...
                                                       initialLearnRate_unet);
        
        
        
        % Display the training progress.
        subplot(1,2,2)
        addpoints(lineLossTrain2,iteration,loss2)
        if mod(iteration,validationFrequency) == 0 || iteration == 1
            shuffle(mbqVm)
            [dlXValid,dlYValid] = next(mbqVm);
            dlYpred=predict(dlnet2,dlXValid);
            Validloss2 = modelPredictions(dlnet2,dlXValid,dlYValid);
            addpoints(lineLossValid2,iteration,Validloss2)
            % Tile and rescale the images in the range [0 1].
            I = imtile(extractdata(dlYpred));
            I = rescale(I);
            subplot(1,2,1);
            imshow(I,[]);
            xticklabels([]);
            yticklabels([]);
            title("Generated Images");
        end
        D = duration(0,0,toc(start),'Format','hh:mm:ss');
        title("U-NET Epoch: " + epoch + ", Iteration: " + iteration + ", Elapsed: " + string(D))
        drawnow
        
    end
end
end
%% Evaluate
Labels=single(lbl_Test); 
Pre=zeros(size(lbl_Test),'single');
ccTest1 = [];
ssimval1 = [];
peaksnr1 = [];

Pre2=zeros(size(lbl_Test),'single');
ccTest2 = [];
ssimval2= [];
peaksnr2 = [];
nbytes=0;
for i =1:size(img_Test,4)
    dlYPred= predict(dlnet1,dlarray(single(img_Test(:,:,1,i)),'SSCB'));
    dlYPred=reshape(dlYPred,64,64);
    dlYPred(dlYPred<0)=0;
    pre=(gather(extractdata(dlYPred./max(dlYPred,[],'all')*255)));
    Pre(:,:,:,i)=pre;

    %NPCC
    rou=corrcoef(Labels(:,:,1,i),Pre(:,:,1,i));
    ccTest1=[ccTest1 rou(1,2)];
    %SSIM
    ssimvaltemp=ssim(uint8(Labels(:,:,1,i)),uint8(Pre(:,:,1,i)));
    ssimval1 = [ssimval1 ssimvaltemp];
    disp(i)
    %PSNR
    psnrtemp= psnr(uint8(Pre(:,:,1,i)),uint8(Labels(:,:,1,i)));
    peaksnr1 =[peaksnr1 psnrtemp];
    
    if unet_skip==1
        dlYPred= predict(dlnet2,dlarray(dlYPred,'SSCB'));
        pre2=(gather(extractdata(dlYPred./max(dlYPred,[],'all')*255)));
        Pre2(:,:,:,i)=pre2;
        %NPCC
        rou=corrcoef(Labels(:,:,1,i),Pre2(:,:,1,i));
        ccTest2=[ccTest2 rou(1,2)];
        %SSIM
        ssimvaltemp=ssim(uint8(Labels(:,:,1,i)),uint8(Pre2(:,:,1,i)));
        ssimval2 = [ssimval2 ssimvaltemp];
        %PSNR
        psnrtemp= psnr(uint8(Pre2(:,:,1,i)),uint8(Labels(:,:,1,i)));
        peaksnr2 =[peaksnr2 psnrtemp];
    end
    fprintf(repmat('\b',1,nbytes)); 
    nbytes = fprintf([num2str(i) '/' num2str(size(img_Test,4))]);
end

fprintf('\n');
fprintf("FCN results:\n");
fprintf('NPCC of Test dataset is:%.3f', sum(ccTest1)./size(img_Test,4));
fprintf('\n');
fprintf('SSIM of Test dataset is:%.3f', sum(ssimval1)./size(img_Test,4));
fprintf('\n');
fprintf('PSNR of Test dataset is:%.3f', sum(peaksnr1)./size(img_Test,4));
fprintf('\n');

if unet_skip==1
    fprintf("Unet results:\n");
    fprintf('NPCC of Test dataset is:%.3f', sum(ccTest2)./size(img_Test,4));
    fprintf('\n');
    fprintf('SSIM of Test dataset is:%.3f', sum(ssimval2)./size(img_Test,4));
    fprintf('\n');
    fprintf('PSNR of Test dataset is:%.3f', sum(peaksnr2)./size(img_Test,4));
    fprintf('\n');
end
%% Save Trained Net
[~,~]=mkdir('TrainedNet');
% save('TrainedNet/fcn+unet-10k','dlnet1','dlnet2');

%% Test Example
k=randperm(size(img_Test,4),5);
% k=[1592,711,1019,1528,418];
figure
tiledlayout(2,5,'TileSpacing','none')
for i=k
    nexttile
    imshow(Labels(:,:,1,i),[])
end
title('Label')
for j=k
    nexttile
    imshow(Pre(:,:,1,j),[])
end
title('Prediction')

end
%% Loss Function
function loss = LossFcn(Yp,Y)
    loss = mseLoss(Yp,Y);
%     +SSIMLoss(Yp,Y)   
end

% normolized mse
function loss = mseLoss(Yp,Y)

Yp=dlarray(Yp,'SSCB');
for i = 1:size(Y,4)
    Y(:,:,1,i)=Y(:,:,1,i)./sqrt(sum(Y(:,:,1,i).^2,'all'));
    Yp(:,:,1,i)=Yp(:,:,1,i)./sqrt(sum(Yp(:,:,1,i).^2,'all')); 
end

loss = mse(Yp,Y);
end

% ssim

function ssim_loss=SSIMLoss(predict,truth)
dynmRange = diff(extractdata(getrangefromclass(truth).*max(truth,[],'all')));
C = [(0.01*dynmRange).^2 (0.03*dynmRange).^2 ((0.03*dynmRange).^2)/2];

radius=1.5;
filtRadius = ceil(radius*3); % 3 Standard deviations include >99% of the area.
filtSize = 2*filtRadius + 1;

h = fspecial('gaussian',filtSize,radius);
gaussFilterFcn = @(X)dlconv(X,h,0,'Padding','same');

mux2 = gaussFilterFcn(truth);
muy2 = gaussFilterFcn(predict);
muxy = mux2.*muy2;
mux2 = mux2.^2;
muy2 = muy2.^2;

sigmax2 = max(gaussFilterFcn(truth.^2) - mux2,0);
sigmay2 = max(gaussFilterFcn(predict.^2) - muy2,0);

sigmaxy = gaussFilterFcn(truth.*predict) - muxy;

num = (2*muxy + C(1)).*(2*sigmaxy + C(2));
den = (mux2 + muy2 + C(1)).*(sigmax2 + sigmay2 + C(2));
ssimmap = num./den;
ssim_loss =mean(ssimmap(:));
ssim_loss = -log(ssim_loss);
end

%% Calculate Gradient 
function [gradients,state,loss] = modelGradients(dlnet,dlX,Y)

[dlYPred,state] = forward(dlnet,dlX);

loss = LossFcn(dlYPred,Y);
gradients = dlgradient(loss,dlnet.Learnables);

loss = double(gather(extractdata(loss)));

end
%% Validation Loss
function Validloss = modelPredictions(dlnet,dlXValid,dlYValid)
    
    Validloss=0;
    i=0;
%     while hasdata(mbq)
        
    
        dlYPred = predict(dlnet,dlXValid);
        loss = LossFcn(dlYPred,dlYValid);
        i=i+1;
        Validloss = Validloss+double(gather(extractdata(loss)));
%     end
    
    Validloss = Validloss./i;
end
%% minibatch process
function [X,Y] = preprocessMiniBatch(XCell,YCell)
    
    % Extract image data from cell and concatenate
    X = cat(4,XCell{:});
    % Extract label data from cell and concatenate
    Y = cat(4,YCell{:});

end
