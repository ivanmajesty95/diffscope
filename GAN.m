% This is for GAN training

% Author: Ma, yifan (yifan.ma@mailbox.tu-dresden.de)
% Date: 14.07.2021
% The MIT License
% Copyright (c) [2021] Ma yifan
for i=1:1
% clc
clear
close all
%% Variables
G_file="Network/UNet_Filter64.mat"; %Generator
% lgraph - input networks
D_file="Network/DisNet.mat"; % Discriminator
% lgraphDiscriminator - input networks

global ADVERSARIAL_LOSS_FACTOR PIXEL_LOSS_FACTOR SIMILARITY_FACTOR

ADVERSARIAL_LOSS_FACTOR = 0.2;
PIXEL_LOSS_FACTOR = 1;
SIMILARITY_FACTOR = 0;
%% Load Dataset
load Dataset/mnist_L.mat
% img_Trn : Training images
% img_Vld : Validation ima  ges
% img_Test: Test images
% lbl_Trn : Training labels
% lbl_Vld : Validation labels
% lbl_Test: Test labels
inputsize=size(img_Trn,1:3);

% layers = 1;
dsXTrain = arrayDatastore(img_Trn,'IterationDimension',4);
dsYTrain = arrayDatastore(lbl_Trn,'IterationDimension',4);
dsTrain = combine(dsXTrain,dsYTrain);

dsXVld = arrayDatastore(img_Vld,'IterationDimension',4);
dsYVld = arrayDatastore(lbl_Vld,'IterationDimension',4);
dsVld = combine(dsXVld,dsYVld);

clear dsXTrain dsYTrain dsXVld dsYVld img_Trn lbl_Trn
%% Load Generator 
load(G_file);
dlnetGenerator = dlnetwork(lgraph);
%% Load Trained Net
% load TrainedNet/UnetTC.mat
% dlnetGenerator=dlnet;
%% Load Discriminator
load(D_file)
dlnetDiscriminator = dlnetwork(lgraphDiscriminator);
%% Training Options
numEpochs = 100;
miniBatchSize = 80;
ValidBatchSize= 40;
GenlearnRate = 1e-3;
DislearnRate = 1e-5;
gradientDecayFactor = 0.2;
squaredGradientDecayFactor = 0.999;
flipFactor = 0;
validationFrequency = 200;

mbq = minibatchqueue(dsTrain,...
    'MiniBatchSize',miniBatchSize,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});
mbqV = minibatchqueue(dsVld,...
    'MiniBatchSize',ValidBatchSize,...
    'MiniBatchFcn',@preprocessMiniBatch,...     
    'MiniBatchFormat',{'SSCB','SSCB'});

trailingAvgGenerator = [];
trailingAvgSqGenerator = [];
trailingAvgDiscriminator = [];
trailingAvgSqDiscriminator = [];
%% Figure Options
f = figure;
f.Position(3) = 2*f.Position(3);
imageAxes = subplot(1,2,1);
scoreAxes = subplot(1,2,2);
lineScoreGenerator = animatedline(scoreAxes,'Color',[0 0.447 0.741]);
lineScoreDiscriminator = animatedline(scoreAxes, 'Color', [0.85 0.325 0.098]);
legend('Generator','Discriminator');
ylim([0 1])
xlabel("Iteration")
ylabel("Score")
grid on
%% Training Loops 
iteration = 0;
start = tic;

% Loop over epochs.
for epoch = 1:numEpochs
    
    % Reset and shuffle datastore.
    shuffle(mbq);
    
    % Loop over mini-batches.
    while hasdata(mbq)
        iteration = iteration + 1;
        
        % Read mini-batch of data.
        [dlX, dlY] = next(mbq);

        % Evaluate the model gradients and the generator state using
        % dlfeval and the modelGradients function listed at the end of the
        % example.
        [gradientsGenerator, gradientsDiscriminator, stateGenerator, scoreGenerator, scoreDiscriminator] = ...
            dlfeval(@modelGradients, dlnetGenerator, dlnetDiscriminator, dlY, dlX, flipFactor);
        dlnetGenerator.State = stateGenerator;
        
        % Update the discriminator network parameters.
        [dlnetDiscriminator,trailingAvgDiscriminator,trailingAvgSqDiscriminator] = ...
            adamupdate(dlnetDiscriminator, gradientsDiscriminator, ...
            trailingAvgDiscriminator, trailingAvgSqDiscriminator, iteration, ...
            DislearnRate, gradientDecayFactor, squaredGradientDecayFactor);
        
        % Update the generator network parameters.
        [dlnetGenerator,trailingAvgGenerator,trailingAvgSqGenerator] = ...
            adamupdate(dlnetGenerator, gradientsGenerator, ...
            trailingAvgGenerator, trailingAvgSqGenerator, iteration, ...
            GenlearnRate, gradientDecayFactor, squaredGradientDecayFactor);
        
        % Every validationFrequency iterations, display batch of generated images using the
        % held-out generator input.
        if mod(iteration,validationFrequency) == 0 || iteration == 1
            % Generate images using the held-out generator input.
            shuffle(mbqV);
            [dlXValid,dlYValid] = next(mbqV);
            dlXGeneratedValidation = predict(dlnetGenerator,dlXValid);
            
            % Tile and rescale the images in the range [0 1].
            I = imtile(extractdata(dlXGeneratedValidation));
            I = rescale(I);
            
            % Display the images.
            subplot(1,2,1);
            imshow(I,[]);
            xticklabels([]);
            yticklabels([]);
            title("Generated Images");
        end
        
        % Update the scores plot.
        subplot(1,2,2)
        addpoints(lineScoreGenerator,iteration,...
            double(gather(extractdata(scoreGenerator))));
        
        addpoints(lineScoreDiscriminator,iteration,...
            double(gather(extractdata(scoreDiscriminator))));
        
        % Update the title with training progress information.
        D = duration(0,0,toc(start),'Format','hh:mm:ss');
        title(...
            "Epoch: " + epoch + ", " + ...
            "Iteration: " + iteration + ", " + ...
            "Elapsed: " + string(D))
        
        drawnow
    end
end
%% Images Test
Label=lbl_Test; 
Pre=zeros(size(lbl_Test),'single');
ccTest = [];
ssimval = [];
peaksnr = [];
for i =1:size(img_Test,4)
    dlYPred= predict(dlnetGenerator,dlarray(single(img_Test(:,:,1,i)),'SSCB'));
    Pre(:,:,:,i) = dlYPred;
    Pre(:,:,:,i)=single(gather(extractdata(dlYPred)));
    %NPCC
    rou=corrcoef(single(Label(:,:,1,i)),single(Pre(:,:,1,i)));
    ccTest=[ccTest rou(1,2)];
    %SSIM
    ssimvaltemp=ssim(uint8(Label(:,:,1,i)),uint8(Pre(:,:,1,i)));
    ssimval = [ssimval ssimvaltemp];
    %PSNR
    psnrtemp= psnr(uint8(Label(:,:,1,i)),uint8(Pre(:,:,1,i)));
    peaksnr =[peaksnr psnrtemp];
end

fprintf('NPCC of Test dataset is:%.3f', sum(ccTest)./size(img_Test,4));
fprintf('\n');
fprintf('SSIM of Test dataset is:%.3f', sum(ssimval)./size(img_Test,4));
fprintf('\n');
fprintf('PSNR of Test dataset is:%.3f', sum(peaksnr)./size(img_Test,4));
fprintf('\n');
%% Save Trained Net
[~,~]=mkdir('TrainedNet');
% save('TrainedNet/GAN_L_UNetC','dlnet');
% save('TrainedNet/GAN-10k','dlnetGenerator','dlnetDiscriminator');

%% Examplar
% k=randperm(size(img_Test,4),5);
k=[933,600,600,600,600];
figure
tiledlayout(2,5,'TileSpacing','none')
for i=k
    nexttile
    imshow(Label(:,:,1,i),[])
end
title('Label')
for j=k
    nexttile
    imshow(Pre(:,:,1,j),[])
end
title('Prediction')


end
%% Gradient Function
function [gradientsGenerator, gradientsDiscriminator, stateGenerator, scoreGenerator, scoreDiscriminator] = ...
    modelGradients(dlnetGenerator, dlnetDiscriminator, dlY, dlX, flipFactor)

global ADVERSARIAL_LOSS_FACTOR PIXEL_LOSS_FACTOR SIMILARITY_FACTOR

% Calculate the predictions for real data with the discriminator network.
dlYPred = forward(dlnetDiscriminator, dlX, dlY);
% Calculate the predictions for generated data with the discriminator network.
[dlXGenerated,stateGenerator] = forward(dlnetGenerator,dlX);
dlYPredGenerated = forward(dlnetDiscriminator, dlX, dlXGenerated);

% Convert the discriminator outputs to probabilities.
probGenerated = sigmoid(dlYPredGenerated);
probReal = sigmoid(dlYPred);

% Calculate the score of the discriminator.
scoreDiscriminator = (mean(probReal) + mean(1-probGenerated)) / 2;

% Calculate the score of the generator.
scoreGenerator = mean(probGenerated);

% Randomly flip a fraction of the labels of the real images.
numObservations = size(probReal,4);
idx = randperm(numObservations,floor(flipFactor * numObservations));

% Flip the labels.
probReal(:,:,:,idx) = 1 - probReal(:,:,:,idx);

% Calculate the GAN loss.
lossDiscriminator = ganDLoss(probReal,probGenerated);
lossGenerator=ADVERSARIAL_LOSS_FACTOR.*(1-scoreGenerator)+...
           PIXEL_LOSS_FACTOR.*PixelLoss(dlY, dlXGenerated)+...
           SIMILARITY_FACTOR.*SSIMLoss(dlY, dlXGenerated);

% For each network, calculate the gradients with respect to the loss.
gradientsGenerator = dlgradient(lossGenerator, dlnetGenerator.Learnables,'RetainData',true);
gradientsDiscriminator = dlgradient(lossDiscriminator, dlnetDiscriminator.Learnables);

end
%% Loss Function
function lossDiscriminator = ganDLoss(probReal,probGenerated)

% Calculate the loss for the discriminator network.
lossDiscriminator = -mean(log(probReal)) - mean(log(1-probGenerated));

% Calculate the loss for the generator network.
% lossGenerator = -mean(log(probGenerated));

end
%% Pixel Loss https://github.com/manumathewthomas/ImageDenoisingGAN
function pixel_loss=PixelLoss(truth,predict)


for i = 1:size(truth,4)
    truth(:,:,1,i)=truth(:,:,1,i)./sqrt(sum(truth(:,:,1,i).^2,'all'));
    predict(:,:,1,i)=predict(:,:,1,i)./sqrt(sum(predict(:,:,1,i).^2,'all')); 
end

pixel_loss = mse(predict,truth);
end
%% SSIM Loss
function ssim_loss=SSIMLoss(truth,predict)
dynmRange = 255;
C = [(0.01*dynmRange).^2 (0.03*dynmRange).^2 ((0.03*dynmRange).^2)/2];

radius=1.5;
filtRadius = ceil(radius*3); % 3 Standard deviations include >99% of the area.
filtSize = 2*filtRadius + 1;

h = fspecial('gaussian',filtSize,radius);
gaussFilterFcn = @(X)dlconv(X,h,0,'Padding','same');

mux = gaussFilterFcn(truth);
muy2 = gaussFilterFcn(predict);
muxy = mux.*muy2;
mux2 = mux.^2;
muy2 = muy2.^2;

sigmax2 = max(gaussFilterFcn(truth.^2) - mux2,0);
sigmay2 = max(gaussFilterFcn(predict.^2) - muy2,0);

sigmaxy = gaussFilterFcn(truth.*predict) - muxy;

num = (2*muxy + C(1)).*(2*sigmaxy + C(2));
den = (mux2 + muy2 + C(1)).*(sigmax2 + sigmay2 + C(2));
ssimmap = num./den.*truth/255;
ssim_loss =mean(ssimmap(ssimmap~=0));
ssim_loss = 1-ssim_loss;
end
%% minibatch process
function [X,Y] = preprocessMiniBatch(XCell,YCell)
    
    % Extract image data from cell and concatenate
    X = cat(4,XCell{:});
    % Extract label data from cell and concatenate
    Y = cat(4,YCell{:});

end