<!--
 * @Author: MA yifan
 * @Date: 2021-08-19 22:30:49
 * @LastEditTime: 2021-11-28 19:47:34
 * @LastEditors: MA, yifan
 * @Description: 
 * @FilePath: \undefinedf:\TUD\Module\SA\Diffscope\README.md
 * The MIT License (MIT) Copyright (c) [2021] Ma yifan
-->
![APM](https://img.shields.io/apm/l/vim-mode?color=informational&style=flat)
# Diffscope
    
**This project is closed**

<!-- ## Table of Contents -->

<!-- vim-markdown-toc GitLab -->

<!-- * [Description](#description)
    * [Main Features](#main-features)
* [Installation](#installation)
* [Upgrade ComNetsEmu and Dependencies](#upgrade-comnetsemu-and-dependencies)
* [Run the Docker-in-Docker example](#run-the-docker-in-docker-example)
* [File Catalog](#file-catalog)
* [Development Guide and API Documentation](#development-guide-and-api-documentation) -->


<!-- vim-markdown-toc -->
## Authors and acknowledgment
Author: Ma, yifan

## Description
This project is used for my studienarbeit - "Vergleich von neuronalen Netzen
zur Bildgebung mit Diffusorendoskopen". You can find the detail in Studienarbeit.pdf

## Support
Especially thanks for my tutor - M.Sc. Julian Lich and some other students as well as  researchers in MST group of Faculty EuI, TU Dresden who work together on this project.

## Details of documents

- Dataset

    This document is used to store the dataset in training. mnist_L_5000 is the example dataset containing 5000 images with large digits generated from MNIST database.

    Test files are the single images for testing. 32 denotes for the receptive field of speckle patterns applied for reconstruction. the normal is 64x64.

- DNN_Reshape_128
  
    This document contains the speckle patterns form Kobanian's dataset. please don't change it.

- ExpData
  
    dataset from Marco's experiment.

- MNIST-Dataset
  
    MNIST database: http://yann.lecun.com/exdb/mnist/

    Fashion-MNIST database: https://github.com/zalandoresearch/fashion-mnist
    please put these file under corresponding documents and run the python file to convert them into .png

- Network

    This file stores the to be trained networks

    Unet_Filter64 is the U-net applying GAN.
    DisNet is the discriminator in GAN.
    DenseNet is Dense-U-Net.

- Trained Networks

    10k denotes the dataset with 10k images, normal one has 5k.