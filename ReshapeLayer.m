classdef ReshapeLayer < nnet.layer.Layer & nnet.layer.Formattable
    properties
        % Output size
        OutputSize
    end
    
    methods
        function layer=ReshapeLayer(outputSize,name)
            layer.OutputSize = outputSize;
            layer.Name=name;
        end
        
        function Z = predict(layer,X)
            % Forward input data through the layer at prediction time and
            % output the result.
            %
            % Inputs:
            %         layer       - Layer to forward propagate through
            %         X           - Input data
            % Outputs:
            %         Z           - Outputs of layer forward function
            
            % Layer forward function for prediction goes here.
            outputSize = layer.OutputSize;
            Z = reshape(X, outputSize(1), outputSize(2), outputSize(3), []);
            Z = dlarray(Z,'SSCB');
        end
    end
end