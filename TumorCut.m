% Author Ma, yifan (yifan.ma@mailbox.tu-dresden.de)
% The MIT License (MIT) 
% Copyright (c) [2021] Ma yifan

% The input size of object plane is 64x64
% the output size of camera is 128x128

clc
clear
close all

%% Variables
L_pix=64; % pixel Labels
i_pix=52;
I_pix=128;
relu=1; % 1 for set (elements = thr) = 0
thr=30; % background value
color_mode=2;% RGB
img_num=5000;
%% File path setting
path='Tumors'; % path to tumor images
imgList=dir(fullfile(path,'*/*/*.tif'));
max_num=size(imgList,1);
%% Create Training Objects in multiple layers
Labels=zeros([L_pix L_pix 1],'single');
nbytes=0;
fprintf('Pre-process the images...\n');
j=0;
for i=1:max_num
    digimg=imread(fullfile(imgList(i).folder,imgList(i).name)); % load digit image
    digres=digimg(:,:,color_mode);
    digres=imresize(digres,0.5);
    dig1=digres(1:i_pix,:);
    dig2=digres(i_pix+1:end,:);
    if size(dig1,1)==size(dig1,2)
        if max(dig1,[],'all')>200
            if relu ==1
                dig1(dig1<=thr)=0;
            end
            padsize =[L_pix-i_pix L_pix-i_pix];
            dig1=padarray(dig1,padsize/2,0);
            j=j+1; 
            Labels(:,:,:,j)=dig1;
        end
    
        if max(dig2,[],'all')>200
            if relu ==1
                dig2(dig2<=thr)=0;
            end
            padsize =[L_pix-i_pix L_pix-i_pix];
            dig2=padarray(dig2,padsize/2,0);
            j=j+1;
            Labels(:,:,:,j)=dig2;
        end
    end
    fprintf(repmat('\b',1,nbytes)); 
    nbytes = fprintf([num2str(i) '/' num2str(max_num)]);
end

%% Generate Speckle Pattern 
path=fullfile('DNN_Reshape_128','8_zdiff=400um'); % path to speckle images

Images=zeros(I_pix,'single');
num_o = randperm(j,img_num);
nbytes=0;
fprintf('Loading...\n');
for i=1:num_img
    Label=Labels(:,:,:,i);
    %rotation
    angle=randi(360)-1;
    Label=imrotate(Label,angle,'bilinear','crop');
    %translation
    Label=circshift(Label,[randi(L_pix) randi(L_pix)]);
    
    Image = zeros(I_pix);
    for x=0:L_pix-1
        for y=0:L_pix-1
            if Label(x+1,y+1)~=0
                num = num2str((III-1)*L_pix^2+ij*L_pix+ii+(obj_layers-1)*4096+1);
                while length(num)<4
                    num = ['0' num];
                end
                Image = Image + Label(x+1,y+1).*single(imread(fullfile(path,strcat(num,'.png'))));
            end
        end
    end
    Image(:,:) = 255*Image(:,:)/max(max(Image(:,:)));

    Images(:,:,:,i) = Image;
    
    fprintf(repmat('\b',1,nbytes)); 
    nbytes = fprintf([num2str(i) '/' num2str(num_img)]);
end
%% Example
% figure
% subplot(1,2,1)
% imshow(Images(:,:,1,15),[0 255])
% title('Input')
% subplot(1,2,2)
% imshow(Labels(:,:,1,15),[0 255])
% title('Label')

%% Save Dataset
[~,~]=mkdir('Dataset');
% save('Dataset/fashion_L_trn','Images','Labels');% Change the file name for different data
