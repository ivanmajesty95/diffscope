% Author: Ma, yifan (yifan.ma@mailbox.tu-dresden.de)
% Date: 17.06.2021
% The MIT License
% Copyright (c) [2021] Ma yifan
clc
clear
close all

%% Variables Setting
p_Trn =0.8; % proportion of traning in a_Trn
p_Vld=0.1;
%% Load raw Dataset
load Dataset/Tumor.mat

a_Trn=size(Images,4); % amount for training and validation

img_Trn   = Images(:,:,:,1:fix(p_Trn*a_Trn));
img_Vld  = Images(:,:,:,fix(p_Trn*a_Trn)+1:fix((p_Trn+p_Vld)*a_Trn));
img_Test  = Images(:,:,:,fix((p_Trn+p_Vld)*a_Trn)+1:end);
lbl_Trn  = Labels(:,:,:,1:fix(p_Trn*a_Trn));
lbl_Vld  = Labels(:,:,:,fix(p_Trn*a_Trn)+1:fix((p_Trn+p_Vld)*a_Trn));
lbl_Test  = Labels(:,:,:,fix((p_Trn+p_Vld)*a_Trn)+1:end);

%% Save Dataset
[~,~]=mkdir('Dataset');
% save('Dataset/mnist_L','img_Trn','img_Vld','img_Test','lbl_Trn','lbl_Vld','lbl_Test','-v7.3');
